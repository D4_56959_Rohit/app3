const express = require('express')
const mysql = require('mysql2')

const app = express()



const openconnection = () => {
    const connection = mysql.createConnection({
        host: 'demodb',
        port: 3306,
        user: 'root',
        password: 'root',
        database: 'dockerdb'
    })

    connection.connect()

    return connection

}

app.use(express.json())

app.get('/', (request, response) => {
    
    const connection = openconnection()

    const statement = `select * from user`

    connection.query(statement, (error,data) => {
        connection.end()
        if (error)
            response.send(error)
        else 
            response.send(data)
    })
})

app.post('/poost', (request, response) => {

const {name, email, password} = request.body

const connection = openconnection()

const statement = `insert into user (name,email,password) values('${name}','${email}','${password}')`

connection.query(statement, (error,result) => {
    connection.end()
    if(error)
        response.send(error)
    else
        response.send(result)

})
})

app.put('/update/:id',(request,response) => {
    const {id} = request.params
    const {email} = request.body

    const connection = openconnection()

    const statement = `update user set email='${email}' where id=${id}`

    connection.query(statement, (error,result) => {
        connection.end()
        if(error)
            response.send(error)
        else
            response.send(result)
    })
})

app.delete('/delete/:id',(request,response) => {
    const {id} =request.params
    
    const connection = openconnection()

    const statement = `delete from user where id=${id}`

    connection.query(statement,(error,result) => {
        connection.end()
        if(error)
             response.send(error)
         else
            response.send(result)

    })


})

app.listen(4000, () => {
    console.log(`server started on port 4000`)
})